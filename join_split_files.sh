#!/bin/bash

cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_product/app/OplusCamera/OplusCamera.apk
rm -f my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat vendor_bootimg/10_dtbdump_;_105,.dtb.* 2>/dev/null >> vendor_bootimg/10_dtbdump_;_105,.dtb
rm -f vendor_bootimg/10_dtbdump_;_105,.dtb.* 2>/dev/null
cat my_region/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_region/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_region/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_region/app/Photos/Photos.apk.* 2>/dev/null >> my_region/app/Photos/Photos.apk
rm -f my_region/app/Photos/Photos.apk.* 2>/dev/null
cat my_region/app/Browser/Browser.apk.* 2>/dev/null >> my_region/app/Browser/Browser.apk
rm -f my_region/app/Browser/Browser.apk.* 2>/dev/null
cat my_region/priv-app/KeKeThemeSpace/KeKeThemeSpace.apk.* 2>/dev/null >> my_region/priv-app/KeKeThemeSpace/KeKeThemeSpace.apk
rm -f my_region/priv-app/KeKeThemeSpace/KeKeThemeSpace.apk.* 2>/dev/null
cat my_region/priv-app/Messages/Messages.apk.* 2>/dev/null >> my_region/priv-app/Messages/Messages.apk
rm -f my_region/priv-app/Messages/Messages.apk.* 2>/dev/null
cat my_region/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null >> my_region/del-app-pre/Facebook/Facebook.apk
rm -f my_region/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat odm/lib64/libstblur_capture_api.so.* 2>/dev/null >> odm/lib64/libstblur_capture_api.so
rm -f odm/lib64/libstblur_capture_api.so.* 2>/dev/null
cat odm/lib64/libarcsoft_super_night_raw.so.* 2>/dev/null >> odm/lib64/libarcsoft_super_night_raw.so
rm -f odm/lib64/libarcsoft_super_night_raw.so.* 2>/dev/null
cat my_stock/app/Aod/Aod.apk.* 2>/dev/null >> my_stock/app/Aod/Aod.apk
rm -f my_stock/app/Aod/Aod.apk.* 2>/dev/null
cat my_stock/del-app/OplusVideoEditor/OplusVideoEditor.apk.* 2>/dev/null >> my_stock/del-app/OplusVideoEditor/OplusVideoEditor.apk
rm -f my_stock/del-app/OplusVideoEditor/OplusVideoEditor.apk.* 2>/dev/null
cat my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat my_stock/priv-app/Mms/Mms.apk.* 2>/dev/null >> my_stock/priv-app/Mms/Mms.apk
rm -f my_stock/priv-app/Mms/Mms.apk.* 2>/dev/null
cat my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> my_stock/priv-app/OppoGallery2/OppoGallery2.apk
rm -f my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
